using Microsoft.AspNetCore.Mvc;

namespace VeterinaryLife.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class HomeController : ControllerBase
{
    public HomeController()
    {

    }

    [HttpGet("health-heck")]
    public IActionResult HealthCheck()
    {
        return Ok();
    }
}
