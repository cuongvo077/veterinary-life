namespace VeterinaryLife.Domain.Model
{
    public class Permission : BaseModel
    {
        public string Title { get; set; } = "";
        public string Code { get; set; } = "";
    }
}