namespace VeterinaryLife.Domain.Model
{
    public class Medicine : MedicineBase
    {
        public string Name { get; set; } = "";
        public string Description { get; set; } = "";
        public List<string> ImagePaths { get; set; } = new List<string>();
    }
}