using VeterinaryLife.Api;

var builder = WebApplication.CreateBuilder(args);
var services = builder.Services;
var startup = new Startup(builder, builder.Environment);

startup.AppServices(services);
startup.ConfigureServices(services);

var app = builder.Build();

startup.Configure(app, builder.Environment);

app.Run();
