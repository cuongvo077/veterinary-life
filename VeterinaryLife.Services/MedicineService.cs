using Microsoft.Extensions.Configuration;
using VeterinaryLife.Domain.Model;
using VeterinaryLife.Domain.Model.Request;

namespace VeterinaryLife.Services
{
    public class MedicineService : BaseService<Medicine>, IMedicineService
    {
        public MedicineService(IConfiguration configuration) : base (configuration)
        {

        }

        public async Task<bool> DeleteMedicineAsync(string id)
        {
            var result = await DeleteByIdAsync(id);
            return result;
        }


        public async Task<Medicine?> GetMedicineByIdAsync(string id)
        {
            var result = await GetByIdAsync(id);
            return result;
        }


        public async Task<List<Medicine>> GetMedicinesAsync()
        {
            var result = await GetAllAsync();
            return result;
        }

        public async Task<Medicine?> InsertMedicineAsync(MedicineRequestModel medicine)
        {
            var result = await AddAsync<MedicineRequestModel>(medicine);
            return result;
        }


        public async Task<bool> UpdateMedicineAsync(Medicine medicine)
        {
            var result = await UpdateDataAsync(medicine, medicine.Id);
            return result;
        }
    }
}