using FireSharp.Interfaces;
using FireSharp.Response;
using Microsoft.Extensions.Configuration;
using Serilog;
using VeterinaryLife.Domain.Mapping;
using VeterinaryLife.Domain.Model;

namespace VeterinaryLife.Services
{
    public abstract class BaseService<T> where T : class
    {
        public readonly IFirebaseClient _client;
        public BaseService(IConfiguration configuration)
        {
            var context = new FirebaseContext(configuration);
            _client = context.client;
        }
        public async Task<List<T>> GetAllAsync()
        {
            var className = typeof(T).Name;
            FirebaseResponse response = await _client.GetAsync($"/{className}");
            return !string.IsNullOrEmpty(response.Body) ? MedicineMapping.Maps<T>(response.Body) : new List<T>();
        }
        public async Task<T?> GetByIdAsync(string id)
        {
            var modelName = typeof(T).Name;
            FirebaseResponse response = await _client.GetAsync($"/{modelName}/{id}");
            var data = MedicineMapping.Map<T>(response.Body);
            return data;
        }
        public async Task<T?> AddAsync<R>(R data)
        where R : class
        {
            var modelName = typeof(T).Name;
            PushResponse response = await _client.PushAsync($"{modelName}/", data);
            var id = response.Result.name;
            var info = data.GetType().GetProperty("Id");
            info?.SetValue(data, Convert.ChangeType(id, info.PropertyType));
            SetResponse setResponse = await _client.SetAsync($"{modelName}/{id}", data);
            return MedicineMapping.Map<T>(setResponse.Body);
        }

        public Task AddRangeAsync(List<T> listData)
        {
            throw new NotImplementedException();
        }
        public async Task<bool> UpdateDataAsync(T data, string id)
        {
            try
            {
                var className = typeof(T).Name;
                FirebaseResponse response = await _client.UpdateAsync<T>($"{className}/{id}", data);
                return true;
            }
            catch (Exception ex)
            {
                Log.Logger.Error("UpdateAsync: {0}", ex.Message);
                return false;
            }
        }

        public async Task<bool> DeleteByIdAsync(string id)
        {
            try
            {
                var modelName = typeof(T).Name;
                await _client.DeleteAsync($"{modelName}/{id}");
                return true;
            }
            catch (Exception ex)
            {
                Log.Logger.Error("DeleteByIdAsync: {0}", ex.Message);
                return false;
            }
        }
    }
}