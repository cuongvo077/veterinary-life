using Microsoft.AspNetCore.Mvc;
using VeterinaryLife.Domain.Model;
using VeterinaryLife.Domain.Model.Request;
using VeterinaryLife.Services;

namespace VeterinaryLife.Api.Controllers;

[ApiController]
[Route("[controller]")]

public class MedicineController : ControllerBase
{
    private readonly IMedicineService _medicineService;
    private readonly ILogger<MedicineController> _logger;

    public MedicineController(ILogger<MedicineController> logger, IMedicineService medicineService)
    {
        _logger = logger;
        _medicineService = medicineService;
    }

    [HttpGet("all")]
    public async Task<IActionResult> GetAllAsync()
    {
        var result = await _medicineService.GetMedicinesAsync();
        return Ok(result);
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetByIdAsync(string id)
    {
        var result = await _medicineService.GetMedicineByIdAsync(id);
        return Ok(result);
    }
    [HttpPut]
    public async Task<IActionResult> UpdateAsync([FromBody] Medicine medicine)
    {
        var result = await _medicineService.UpdateMedicineAsync(medicine);
        return Ok(result);
    }
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteAsync(string id)
    {
        var result = await _medicineService.DeleteMedicineAsync(id);
        return Ok(result);
    }
    [HttpPost]
    public async Task<IActionResult> InsertAsync([FromBody] MedicineRequestModel medicine)
    {
        var result = await _medicineService.InsertMedicineAsync(medicine);
        return Ok(result);
    }
}