namespace VeterinaryLife.Domain.Model.Request
{
    public class MedicineRequestModel
    {
        public string Name { get; set; } = "";
        public string Description { get; set; } = "";
        public string Provider { get; set; } = "";
        public string Type { get; set; } = "";
        public decimal SellPrice { get; set; }
        public decimal ImportPrice { get; set; }
        public int ImportCount { get; set; }
        public int SellCount { get; set; }
        public List<string> ImagePaths { get; set; } = new List<string>();
    }
}