using VeterinaryLife.Domain.Model;
using VeterinaryLife.Domain.Model.Request;

namespace VeterinaryLife.Services
{
    public interface IMedicineService
    {
        Task<List<Medicine>> GetMedicinesAsync();
        Task<Medicine?> GetMedicineByIdAsync(string id);
        Task<bool> UpdateMedicineAsync(Medicine medicine);
        Task<bool> DeleteMedicineAsync(string id);
        Task<Medicine?> InsertMedicineAsync(MedicineRequestModel medicine);

    }
}