using System.Runtime.CompilerServices;
using Newtonsoft.Json;

namespace VeterinaryLife.Domain.Mapping
{
    public static class MedicineMapping
    {
        public static List<T> Maps<T>(string body) where T : class
        {
            var objectBody = JsonConvert.DeserializeObject<Dictionary<string, T>>(body);
            var result = objectBody?.Count > 0 ? objectBody.Select(x => x.Value).ToList() : new List<T>();
            return result;
        }
        public static T? Map<T>(string body) where T : class
        {
            var result = JsonConvert.DeserializeObject<T>(body);
            return result;
        }
    }
}