using Serilog;
using VeterinaryLife.Services;

namespace VeterinaryLife.Api
{
    internal class Startup
    {
        private IConfiguration Configuration { get; }
        public IWebHostEnvironment HostingEnvironment { get; }
        public Startup(WebApplicationBuilder builder, IWebHostEnvironment env)
        {
            Configuration = builder.Configuration;
            HostingEnvironment = env;
            builder.Host.UseSerilog((context, configuration) => configuration.ReadFrom.Configuration(context.Configuration));
        }

        public void Configure(WebApplication app, IWebHostEnvironment env)
        {
            // Configure the HTTP request pipeline.
            if (app.Environment.EnvironmentName == "Local" || app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseSerilogRequestLogging();
            app.UseHttpsRedirection();

            app.UseAuthentication();
            app.UseAuthorization();

            app.MapControllers();
        }

        public void AppServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen();
            services.AddMemoryCache();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IMedicineService, MedicineService>();
        }
    }
}