namespace VeterinaryLife.Domain.Model
{
    public abstract class MedicineBase : BaseModel
    {
        public int ImportCount { get; set; }
        public int SellCount { get; set; }
        public string Provider { get; set; } = "";
        public string Type { get; set; } = "";
        public decimal SellPrice { get; set; }
        public decimal ImportPrice { get; set; }
        public int CurrentCount
        {
            get {
                return ImportCount - SellCount;
            }
        }
    }
}