namespace VeterinaryLife.Domain.Model
{
    public class User : BaseModel
    {
        public string Email { get; set; } = "";
        public string UserName { get; set; } = "";
    }
}