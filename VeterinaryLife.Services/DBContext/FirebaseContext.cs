﻿using FireSharp.Config;
using FireSharp.Interfaces;
using Microsoft.Extensions.Configuration;

namespace VeterinaryLife.Services
{
    public class FirebaseContext
    {

        public readonly IFirebaseClient client;
        public FirebaseContext(IConfiguration Configuration)
        {
            var authSecret = Configuration.GetSection("FirebaseConfig:AuthSecret").Value;
            var basePath = Configuration.GetSection("FirebaseConfig:DatabaseURL").Value;
            IFirebaseConfig config = new FirebaseConfig
            {
                AuthSecret = authSecret,
                BasePath = basePath
            };
            client = new FireSharp.FirebaseClient(config);
        }
    }
}
