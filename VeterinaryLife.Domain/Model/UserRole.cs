namespace VeterinaryLife.Domain.Model
{
    public class UserRole : BaseModel
    {
        public string Role { get; set; } = "";
        public List<string> PermissionIds { get; set; } = new List<string>();
    }
}